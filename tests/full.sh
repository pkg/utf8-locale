#!/bin/sh
#
# Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

set -e

# Run the Python test suite, prepare .tox/functional/bin/python3
[ -n "$NO_CLEAN$NO_PYTHON_CLEAN" ] || rm -rf .tox
tox-delay -p all -e unit_tests,functional

# Build the Rust project and run the test suite against it
: "${CARGO:=cargo}"
[ -n "$NO_CLEAN$NO_RUST_CLEAN" ] || $CARGO clean
$CARGO fmt -- --check
$CARGO doc --no-deps
$CARGO build
$CARGO clippy
.tox/functional/bin/python3 -B -u tests/functional.py -p target/debug/u8loc

# Build the Rust project in release mode and run the test suite against it
: "${CARGO:=cargo}"
$CARGO fmt -- --check
$CARGO doc --no-deps --release
$CARGO build --release
$CARGO clippy --release
.tox/functional/bin/python3 -B -u tests/functional.py -p target/release/u8loc

# Build the C project, run the test suite against it, install it locally
[ -n "$NO_CLEAN$NO_C_CLEAN" ] || rm -rf obj
cmake -S . -B obj -DUSE_BDE_CFLAGS=ON -DUSE_WERROR=ON
make -C obj
.tox/functional/bin/python3 -B -u tests/functional.py -p obj/c/u8loc/u8loc

rm -rf temproot
mkdir temproot
make -C obj install DESTDIR="$(pwd)/temproot"
find temproot/ -ls

# All fine?
echo 'Everything seems to be fine!'
